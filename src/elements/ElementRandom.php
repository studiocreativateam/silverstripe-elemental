<?php

namespace studiocreativateam\Elemental\Models;

class ElementRandom extends ElementList
{
    private static $table_name = 'ElementRandom';

    private static $_element;

    public function getElement()
    {
        if (empty(static::$_element)) {
            $elements = $this->Elements()->Elements();
            if ($count = $elements->count()) {
                static::$_element = $elements->toArray()[rand(0, $count - 1)];
            }
        }
        return static::$_element;
    }
}