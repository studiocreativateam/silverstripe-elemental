<?php

namespace studiocreativateam\Elemental\Models;

class ElementList extends \DNADesign\ElementalList\Model\ElementList
{
    use ElementTrait;

    private static $table_name = 'ElementListExt';
}