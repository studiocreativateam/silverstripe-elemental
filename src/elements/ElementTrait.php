<?php

namespace studiocreativateam\Elemental\Models;

trait ElementTrait
{
    public function getStyleVariant()
    {
        $style = $this->Style;
        $styles = $this->config()->uninherited('styles');
        $style = is_array($styles) && in_array($style, $styles) ? strtolower($style) : '';

        $this->extend('updateStyleVariant', $style);

        return $style;
    }

    public function getType()
    {
        $classParts = explode('\\', $this->ClassName);
        return _t(__CLASS__ . '.BlockType', preg_replace('/^Element/', '', array_pop($classParts)));
    }
}