<?php

namespace studiocreativateam\Elemental\Models;

use DNADesign\Elemental\Models\BaseElement;

class ElementYoutube extends BaseElement
{
    use ElementTrait;

    private static $table_name = 'ElementYoutube';

    private static $db = [
        'Playlist' => 'Boolean',
        'YoutubeID' => 'Varchar',
    ];

    public function getSrc()
    {
        return sprintf("https://www.youtube.com/embed/%srel=0", $this->Playlist ? "videoseries?list=" . $this->YoutubeID . "&" : $this->YoutubeID . "?");
    }
}