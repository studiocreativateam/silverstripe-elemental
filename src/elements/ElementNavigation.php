<?php

namespace studiocreativateam\Elemental\Models;

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Security\Permission;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class ElementNavigation extends ElementContent
{
    private static $table_name = 'ElementNavigation';

    private static $has_many = [
        'Items' => ElementNavigationItem::class,
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName('Items');

        if ($this->ID) {
            $config = GridFieldConfig_RecordEditor::create();
            $config->addComponent(new GridFieldOrderableRows('SortOrder'));
            $tabName = ucfirst(_t(ElementNavigationItem::class . '.PLURALNAME', "Items"));
            $grid = new GridField('Items', $tabName, $this->Items(), $config);
            $fields->addFieldToTab('Root.Main', $grid);
        }

        return $fields;
    }
}

class ElementNavigationItem extends DataObject
{
    private static $table_name = 'ElementNavigationItem';

    private static $db = [
        'MenuTitle' => 'Varchar',
        'Link' => 'Varchar(2083)',
        'IsNewWindow' => 'Boolean',
        'SortOrder' => 'Int',
    ];

    private static $has_one = [
        'Parent' => ElementNavigation::class,
        'Page' => SiteTree::class,
    ];

    private static $searchable_fields = [
        'MenuTitle',
        'Page.Title'
    ];

    private static $summary_fields = [
        'MenuTitleNice' => 'Title',
        'Page.Title' => 'Page Title',
        'Link' => 'Link',
        'IsNewWindowNice' => 'Opens in new window?'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName('SortOrder');
        $fields->removeByName('ParentID');
        $fields->removeByName('PageID');

        $fields->fieldByName('Root.Main.MenuTitle')
            ->setDescription('If left blank, will default to the selected page\'s name.');

        $fields->insertBefore(
            TreeDropdownField::create(
                'PageID',
                'Page on this site',
                SiteTree::class
            )->setDescription('Leave blank if you wish to manually specify the URL below.'),
            'Link'
        );

        $fields->addFieldsToTab('Root.Main', $fields->fieldByName('Root.Main.Link'));
        $fields->addFieldToTab('Root.Main', $fields->fieldByName('Root.Main.IsNewWindow'));

        return $fields;
    }

    public function getTitle()
    {
        return $this->MenuTitle;
    }

    public function getMenuTitleNice()
    {
        return strip_tags(preg_replace('/<br(\s\/)?>/', ' ', $this->MenuTitle));
    }

    public function IsNewWindowNice()
    {
        return $this->IsNewWindow ? 'Yes' : 'No';
    }

    public function __get($field)
    {
        $default = parent::__get($field);
        if ($default || $field === 'ID') {
            return $default;
        } else {
            $page = $this->Page();
            if ($page instanceof DataObject) {
                return $page->hasMethod($field) ? $page->$field() : $page->$field;
            }
        }
    }

    public function canView($member = null, $context = array())
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null, $context = array())
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null, $context = array())
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canCreate($member = null, $context = array())
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        if ($this->PageID) {
            $this->Link = null;
        }
    }
}