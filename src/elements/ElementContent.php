<?php

namespace studiocreativateam\Elemental\Models;

class ElementContent extends \DNADesign\Elemental\Models\ElementContent
{
    use ElementTrait;

    private static $table_name = 'ElementContentExt';

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->fieldByName('Root.Main.HTML')->setRows(10);
        return $fields;
    }
}