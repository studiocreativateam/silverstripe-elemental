<?php

namespace studiocreativateam\Elemental\Models;

use SilverStripe\ElementalBlocks\Block\BannerBlock;

class ElementBanner extends BannerBlock
{
    use ElementTrait;

    private static $table_name = 'ElementBannerExt';

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        if (!$this->config()->get('with_cta')) {
            $fields->removeByName('CallToActionLink');
        }
        $fields->fieldByName('Root.Main.File')->setFolderName('banners');
        return $fields;
    }
}