<?php

namespace studiocreativateam\Elemental;

use Page;
use PageController;
use SilverStripe\Core\ClassInfo;

class PageAsBlocks extends Page
{
    private static $table_name = 'PageAsBlocks';
    private static $extensions = [
        ElementalPageExtension::class,
    ];

    protected function getPageDescription($original = false)
    {
        foreach ($this->ElementalArea()->Elements() as $element) {
            if (in_array('DNADesign\Elemental\Models\ElementContent', ClassInfo::ancestry($element))) {
                return $element->HTML;
            }
        }
        return parent::getPageDescription($original);
    }

    public function getShareImage()
    {
        $elements = $this->ElementalArea()->Elements();
        foreach ($elements as $element) {
            if (in_array('SilverStripe\ElementalBlocks\Block\BannerBlock', ClassInfo::ancestry($element))) {
                if (($image = $element->File()) && $image->exists()) {
                    return $image;
                }
            }
        }
        foreach (class_parents($this) as $parent) {
            if ($parent == PageAsBlocks::class) continue;
            if (method_exists($parent, 'getShareImage')) {
                return parent::getShareImage();
            }
        }
    }
}

class PageAsBlocksController extends PageController
{
}