<?php

namespace studiocreativateam\Elemental;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class ElementalPageExtension extends \DNADesign\Elemental\Extensions\ElementalPageExtension
{
    private static $cascade_deletes = [
        'ElementalArea',
    ];

    public function updateCMSFields(FieldList $fields)
    {
        parent::updateCMSFields($fields);

        if ($this->owner->config()->get('with_content')) {
            $content = HTMLEditorField::create("Content", _t(__CLASS__ . '.HTMLEDITORTITLE', "Content", 'HTML editor title'));
            $fields->addFieldToTab('Root.Main', $content, 'ElementalArea');
            $content->addExtraClass('stacked');
            $content->setRows(7);
        }
    }
}