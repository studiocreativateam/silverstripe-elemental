<?php

use SilverStripe\Core\Extension;

class ElementalEditorExtension extends Extension
{
    public function updateGetTypes(&$types)
    {
        if (isset($types['SilverStripe\ElementalBlocks\Block\BannerBlock'])) {
            unset($types['SilverStripe\ElementalBlocks\Block\BannerBlock']);
        }
        if (isset($types['DNADesign\Elemental\Models\ElementContent'])) {
            unset($types['DNADesign\Elemental\Models\ElementContent']);
        }
        if (isset($types['DNADesign\ElementalList\Model\ElementList'])) {
            unset($types['DNADesign\ElementalList\Model\ElementList']);
        }
    }
}