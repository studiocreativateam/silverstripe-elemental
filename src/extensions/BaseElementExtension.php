<?php

namespace studiocreativateam\Elemental;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;
use studiocreativateam\URL;
use studiocreativateam\Utils;

class BaseElementExtension extends DataExtension
{
    public function updateCMSFields(FieldList $fields)
    {
        if ($styles = $fields->fieldByName('Root.Settings.Style')) {
            $source = $this->owner->config()->uninherited('styles');
            if (empty($source)) {
                $fields->removeByName('Styles');
            } else {
                $source = array_combine($source, $source);
                $styles->setSource($source);
            }
        }
    }

    public function getTitleForHash()
    {
        return method_exists($this->owner, 'getTitleForHash') ? $this->owner->TitleForHash : URL::prepareURLSegment($this->owner->Title);
    }

    public function getType()
    {
        $classParts = explode('\\', $this->owner->ClassName);
        return _t(__CLASS__ . '.BlockType', preg_replace('/^Element/', '', array_pop($classParts)));
    }

    public function getTypeClass()
    {
        $classParts = explode('\\', $this->owner->ClassName);
        return preg_replace('/_/', '-', Utils::from_camel_case(preg_replace('/^Element/', '', array_pop($classParts))));
    }

    // public function updateStyleVariant(&$style)
    // {
    //   $style = $this->owner->Style;
    //   $styles = $this->owner->config()->get('styles');
    //   $style = isset($styles[$style]) ? $styles[$style] : '';
    // }
}
