<?php

namespace studiocreativateam\Elemental;

use SilverStripe\View\SSViewer;

class ElementController extends \DNADesign\Elemental\Controllers\ElementController
{
    public function forTemplate()
    {
        $defaultStyles = $this->config()->get('default_styles');
        if ($this->config()->get('include_default_styles') && !empty($defaultStyles)) {
            foreach ($defaultStyles as $stylePath) {
                Requirements::css($stylePath);
            }
        }

        $template = $this->element->config()->get('controller_template');

        $tmp = [
            'type' => 'Layout',
            'studiocreativateam\\Elemental\\' . $template,
            $template,
        ];

        if (!SSViewer::hasTemplate($tmp)) {
            unset($tmp['type']);
        }
        return $this->renderWith($tmp);
    }

    public function HasClass($name)
    {
        $classes = explode(' ', $this->element->ExtraClass);
        $classes[] = $this->element->StyleVariant;
        return in_array($name, $classes);
    }
}